import numpy as np
from ase import Atoms, units
from ase.md import VelocityVerlet
from cmm.tools.helpercalcs import DoubleCalc, SimpleBond

dt = 0.2 
k = 500 * units.kJ / units.mol



atoms =  Atoms('CC', positions=np.array([[-1.00, 0, 0], [1.00, 0, 0]]))

atoms.calc = SimpleBond([[0, 1]], [1.5 * k], [1.5])

dyn = VelocityVerlet(atoms, timestep=0.2 * units.fs,
                     trajectory='uncorrected_verlet.traj', logfile='uncorrected_verlet.log')

dyn.run(10000)
