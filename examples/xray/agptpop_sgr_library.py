''' Build library of dS(q) signals from all possible 
    combinations of all GS and ES MM solvent shell sample
    runs. '''

import glob
import numpy as np
from cmm.xray.sgr import SGr, Damping

datadir = '/home/asod/Dropbox/DTU2/OpenMM/AgPtPOP/md/Phillip_OPT/'
gs_dirs = sorted(glob.glob(datadir + '*_gs_*'))
es_dirs = sorted(glob.glob(datadir + '*_es_*'))

V = 44.493179 * 44.000607 * 44.524986
stoich = {'Ag_u': 1, 'H_u': 8, 'H_v': 5742, 'O_u': 20, 'O_v': 2871, 'P_u': 8, 'Pt_u': 2}

damp = damp = Damping('zederkof', r_cut=15, r_max=20)

all_es = [[] for x in range(len(es_dirs))]
all_gs = [[] for x in range(len(gs_dirs))]

for e, esd in enumerate(es_dirs):
    print(esd)
    es_sgr = SGr(V, damp=damp, delta=False, verbose=False, ignore_elements=['Na'])
    es_sgr.load_rdfs_fromdir(esd)
    es_sgr.set_stoichometry(stoich)
    es_sgr.fit_alphadv2()
    es_s = es_sgr.calculate(es_sgr.rdfs, stoich)
    all_es[e] = (es_s, esd)

np.save('all_es_zkf15-20_advfit2.npy', all_es)

for g, gsd in enumerate(gs_dirs):
    print(gsd)
    gs_sgr = SGr(V, damp=damp, delta=False, verbose=False, ignore_elements=['Na'])
    gs_sgr.load_rdfs_fromdir(gsd)
    gs_sgr.set_stoichometry(stoich)
    gs_sgr.fit_alphadv2()
    gs_s = gs_sgr.calculate(gs_sgr.rdfs, stoich)
    all_gs[g] = (gs_s, gsd)

np.save('all_gs_zkf15-20_advfit2.npy', all_gs)
