import numpy as np
from ase.io import read
from ase import Atoms
from cmm.md.cmmsystems import CMMSystem
from parmed.openmm.reporters import RestartReporter
from simtk.unit import angstroms 
''' An example of running some MD, after you've completed the parametrisation
    using e.g. MCPB.py of ambertools. '''

tag = 'febpy_gs_opls_newindex'
fpath = '/work1/asod/HABIB/FeBPY_MD/'


def nonsolvent_as_ase(sys, ns_idx):
    _, syms = sys.get_nonsolvent_indices(tag='Cl-')
    els = [atom.element._symbol for a, atom in enumerate(sys.topology.atoms()) if a in ns_idx]
    pos = np.array(sys.positions.value_in_unit(angstroms))[ns_idx]
    atoms  = Atoms(els, positions=pos)
    return atoms

# The structure the charges belong to
habib_atoms = read('geo-b3lyp-d3-def2tzvp-singlet.xyz')[:61]

# init
sys = CMMSystem(tag, fpath=fpath)

# load prmtop (and in this case also inpcrd, which means positions will also be
# loaded)
sys.system_from_prmtop('../../data/md/febpy/reindexed/FEBPY_solv.prmtop',
                       inpcrd='../../data/md/febpy/reindexed/FEBPY_solv.inpcrd')

mcbpy_atoms = nonsolvent_as_ase(sys, list(range(61)))
habib_atoms.positions += habib_atoms.positions[0] + mcbpy_atoms.positions[0] # this is enough for aligning

charges = np.genfromtxt('/zhome/c7/a/69784/HABIB/tdb/opt_geoms/BPY/Fe-bipy-LS-b3lyp-d3-def2tzvp-chelpg_for_MM.charges')[:, 1]

### The new FF has REINDEXED itself... But now the stereoisomer is at least the same
new_pos = habib_atoms.get_positions()
new_syms = habib_atoms.get_chemical_symbols()
index_map = [] # first metal is the same
for o, oa in enumerate(mcbpy_atoms):
    old_sym = oa.symbol
    dists = np.linalg.norm(new_pos - oa.position, axis=1)  # all dists from oa to kruppa
    # find the shortest distance to an atom OF THE SAME ELEMENT   
    idx = np.ma.argmin(np.ma.MaskedArray(dists, [ns != old_sym for ns in new_syms]), fill_value=100)
    index_map.append(idx)
        
    print(f'old atom {o}, {old_sym} is closest to new {idx}, {new_syms[idx]}, distance: {dists[idx]}')


ns_idx, syms = sys.get_nonsolvent_indices(tag='Cl-')
solu_idx = [i for i, sym in enumerate(syms) if sym != 'Cl-']
sys.adjust_solute_charges(solu_idx, charges[index_map])

# Lets restrain FeBPY to the positions from the opt, and move Cl- away
ct_idx = [i for i, sym in enumerate(syms) if sym == 'Cl-']
solu_pos = sys.positions[solu_idx].value_in_unit(angstroms)
ct_pos  = sys.positions[ct_idx].value_in_unit(angstroms)
cen = np.mean(solu_pos, 0)
vs = ct_pos - cen
vs /= np.linalg.norm(vs, axis=1)[:, None]
for i, v in enumerate(vs):
    print(np.linalg.norm(ct_pos[i] - cen, axis=0))
    ct_pos[i] += 15 *v
    print(np.linalg.norm(ct_pos[i] - cen, axis=0))

pos = sys.positions.value_in_unit(angstroms)
pos[ct_idx] = ct_pos
pos[solu_idx] = habib_atoms.positions[index_map]

sys.update_positions(pos)
sys.restrain_atoms(ns_idx, 500)

# this time, no charge changing, or adding constraints. So init NPT:
sys.add_barostat()

# and integrator
sys.set_integrator()  

# init sim
sys.init_simulation(platform='CUDA', double=False)

# minimize
sys.minimize()

# MBD:
sys.mbd()

# reporters
sys.standard_reporters(step=250)

restrt = RestartReporter(fpath + f'{tag}.rst7', 1000)
sys.add_reporter(restrt)

# runnnn
sys.run(time_in_ps=50000)
