import rmsd
import numpy as np
from os.path import isdir
from simtk.unit import kilojoule_per_mole, angstroms
import simtk.unit as u
from cmm.md.cmmsystems import CMMSystem
from cmm.tools.parsers import read_chargefile
from parmed.openmm.reporters import RestartReporter
from ase.io import read
from simtk.openmm.openmm import HarmonicAngleForce 


''' Do them all while i sleep. Write restarts out often 
    for further analysis of the nans, And just continue 
    with the next one if it happens. '''

with open('../../data/md/agptpop/es_constrained_chelpg_charges.dat', 'r') as f:
    lines = f.readlines()

versions = [line for line in lines if ': ESP char' in line]
for vers in versions:
    print(vers)

for ii, vers in enumerate(versions):
    ptval = vers.split('Pt')[-1].split('_')[0]
    agval = vers.split('Ag')[-1].split('.log')[0]

    #if ptval != '1.25':  # only do this one first, for comparison
    #    continue
    #if agval != '1.72':
    #    continue

    equilname = f'agptpop_nvt_equil_es_constrainedopt_conandres_noNPT_Pt{ptval}_Ag{agval}'
    produname = f'agptpop_nvt_production_es_constrainedopt_conandres__noNPT_Pt{ptval}_Ag{agval}'
    
    print(produname)    

    fpath = f'../../../data/AgPtPOP/agptpop_es_constrainedopt_conandres_noNPT_Pt{ptval}_Ag{agval}/'

    if isdir(fpath):  # has already run
        print(f'{fpath} already exists, skipping...')
        continue 

    # New charges:
    charges, elmnts = read_chargefile('../../data/md/agptpop/es_constrained_chelpg_charges.dat', f'Pt{ptval}_Ag{agval}')
    charges = charges[:, 1]
    print(f'Sum of charges: {sum(charges)}')
    idx = list(range(len(charges)))

    #### init - already pushed away ions when we start
    sys = CMMSystem(equilname, fpath=fpath)

    sys.system_from_prmtop('../../data/md/agptpop/agptpop_solv_custom.prmtop')

    sys.load_rst7('/zhome/c7/a/69784/openMM/data/AgPtPOP/STARTFROMTHIS/agptpop_nvt_pushaway_Pt1.50_Ag2.25.rst7')

    # set new charges
    sys.adjust_solute_charges(idx, charges)

    # Set positions to xray opted
    ns_idx, syms = sys.get_nonsolvent_indices()
    solu_idx = [i for i, sym in enumerate(syms) if sym != 'Na+']
    pos = sys.positions.value_in_unit(angstroms)
    pos = np.array([[p.x, p.y, p.z] for p in pos])
    old_pos = pos.copy()[solu_idx]
    new_atoms = read('../../data/md/agptpop/agptpop_es_constrained_almost_opt_resequenced.xyz')
    pos_a = new_atoms.get_positions()  # align to gs pos in box first
    pos_t = pos[solu_idx]
    pos_a += -rmsd.centroid(pos_a) + rmsd.centroid(pos_t)  # translate
    U = rmsd.kabsch(pos_a, pos_t)  # rotate
    pos_a = np.dot(pos_a, U)
    pos[solu_idx] = pos_a  # overwrite old gs pos with aligned new pos
    sys.positions = pos * angstroms

    # restrain metals and ct ions to their original positions
    sys.restrain_atoms(ns_idx, 5000)

    # Reset Pt-P and P-O bond d0s to distances in QM xyz
    # as well as loosen them up a bit
    P_idx = [a.index for a in sys.topology.atoms() 
                if (('P' in a.name) and (not 'T' in a.name) and (not 'E' in a.name ))]

    Pt_idx = [a.index for a in sys.topology.atoms() if 'PT' in a.name]
    O_idx = [a.index for a in sys.topology.atoms() if ('O' in a.name and a.index in ns_idx)]

    for pt in Pt_idx:
        for p in P_idx:
            xyz_dist = new_atoms.get_distance(pt, p)
            dist, k, name = sys.get_bond(pt, p)
            if k:
                sys.update_bond(pt, p, xyz_dist * u.angstroms, k)
    for o in O_idx:
        for p in P_idx:
            xyz_dist = new_atoms.get_distance(o, p)
            sys.update_bond(o, p, xyz_dist * u.angstroms)

    ### Also update angles containing metals
    for force in sys.system.getForces():
            if isinstance(force, HarmonicAngleForce):
                abf = force
    me_list = [0, 1, 2]
    for bond in range(abf.getNumAngles()):
        a, b, c, angle, k = abf.getAngleParameters(bond)
        if (a in me_list) or (b in me_list) or (c in me_list):
            angle = angle.in_units_of(u.degrees)._value
            k_kjmolang = k.in_units_of(kilojoule_per_mole / u.degrees**2)._value
            xyz_angle = new_atoms.get_angle(a, b, c)
            print(f'{a:2d},{b:2d},{c:2d}: {syms[a]:3s}-{syms[b]:3s}-{syms[c]:3s}: TOPt: {angle:6.2f} Deg. XYZ: {xyz_angle:6.2f}, k = {k_kjmolang:g} kj/mol*deg**2')

            sys.update_angle(a, b, c, angle=(xyz_angle *u.degrees).in_units_of(u.radians))

    # constrain metal distances
    agpt = np.linalg.norm(pos[0] - pos[2]) * angstroms
    #sys.add_bond(0, 2, agpt, 3000 * kilojoule_per_mole)
    sys.add_constraint(0, 2, agpt)
    for i in range(8622):
        c = sys.system.getConstraintParameters(i) 
        if c[0] == 0:
            print(i, c)
            break
    ptpt = np.linalg.norm(pos[1] - pos[2]) * angstroms
    #sys.add_bond(1, 2, ptpt, 3000 * kilojoule_per_mole)
    sys.add_constraint(1, 2, ptpt)
    #agpt2 = np.linalg.norm(pos[0] - pos[1]) * angstroms
    #sys.add_bond(0, 1, agpt2, 3000 * kilojoule_per_mole)

    # and integrator
    sys.set_integrator(timestep=0.5)  
    sys.integrator.setConstraintTolerance(1e-12)

    # init sim
    sys.init_simulation(platform='CUDA', double=False)

    # reporters
    sys.standard_reporters()
    
    # restart reporter
    restrt = RestartReporter(fpath + f'{equilname}.rst7', 1000) 
    sys.add_reporter(restrt)

    # runnnn
    sys.run(time_in_ps=200)

    ## Production
    sys = CMMSystem(produname, fpath=fpath)
    sys.system_from_prmtop('../../data/md/agptpop/agptpop_solv_custom.prmtop')
    sys.load_rst7(fpath +f'{equilname}.rst7')

    # set new charges
    sys.adjust_solute_charges(idx, charges)

    # restrain metals and ct ions to their original positions
    sys.restrain_atoms(ns_idx, 5000)

    # Reset Pt-P and P-O bond d0s to distances in QM xyz
    # as well as loosen them up a bit
    P_idx = [a.index for a in sys.topology.atoms() 
                if (('P' in a.name) and (not 'T' in a.name) and (not 'E' in a.name ))]

    Pt_idx = [a.index for a in sys.topology.atoms() if 'PT' in a.name]
    O_idx = [a.index for a in sys.topology.atoms() if ('O' in a.name and a.index in ns_idx)]

    for pt in Pt_idx:
        for p in P_idx:
            xyz_dist = new_atoms.get_distance(pt, p)
            dist, k, name = sys.get_bond(pt, p)
            if k:
                sys.update_bond(pt, p, xyz_dist * u.angstroms, k)
    for o in O_idx:
        for p in P_idx:
            xyz_dist = new_atoms.get_distance(o, p)
            sys.update_bond(o, p, xyz_dist * u.angstroms)

    ### Also update angles containing metals
    for force in sys.system.getForces():
            if isinstance(force, HarmonicAngleForce):
                abf = force
    me_list = [0, 1, 2]
    for bond in range(abf.getNumAngles()):
        a, b, c, angle, k = abf.getAngleParameters(bond)
        if (a in me_list) or (b in me_list) or (c in me_list):
            angle = angle.in_units_of(u.degrees)._value
            k_kjmolang = k.in_units_of(kilojoule_per_mole / u.degrees**2)._value
            xyz_angle = new_atoms.get_angle(a, b, c)
            print(f'{a:2d},{b:2d},{c:2d}: {syms[a]:3s}-{syms[b]:3s}-{syms[c]:3s}: TOPt: {angle:6.2f} Deg. XYZ: {xyz_angle:6.2f}, k = {k_kjmolang:g} kj/mol*deg**2')

            sys.update_angle(a, b, c, angle=(xyz_angle *u.degrees).in_units_of(u.radians))

    # constrain metal distances
    agpt = np.linalg.norm(pos[0] - pos[2]) * angstroms
    sys.add_constraint(0, 2, agpt)
    for i in range(8622):
        c = sys.system.getConstraintParameters(i) 
        if c[0] == 0:
            print(i, c)
            break
    ptpt = np.linalg.norm(pos[1] - pos[2]) * angstroms
    sys.add_constraint(1, 2, ptpt)

    # and integrator
    sys.set_integrator(timestep=2.0)  

    # init sim
    sys.init_simulation(platform='CUDA', double=False)

    # reporters
    sys.standard_reporters(step=250)

    # restart reporter
    restrt = RestartReporter(fpath + f'{produname}.rst7', 1000) 
    sys.add_reporter(restrt)

    # runnnn
    sys.run(time_in_ps=10000)
