from cmm.md.cmmsystems import CMMSystem

''' An example of running some MD, after you've completed the parametrisation
    using e.g. MCPB.py of ambertools. '''


# init
sys = CMMSystem('agptpop_gs_npt_gpu', fpath='./agptpop_gs_npt_gpu')

# load prmtop (and in this case also inpcrd, which means positions will also be
# loaded)
sys.system_from_prmtop('../../data/md/agptpop/agptpop_solv_custom.prmtop',
                       inpcrd='../../data/md/agptpop/agptpop_solv_custom.inpcrd')


# this time, no charge changing, or adding constraints. So init NPT:
sys.add_barostat()

# and integrator
sys.set_integrator()  

# init sim
sys.init_simulation()


# minimize
sys.minimize()


# MBD:
sys.mbd()

# reporters
sys.standard_reporters()

# runnnn
sys.run(time_in_ps=200)
