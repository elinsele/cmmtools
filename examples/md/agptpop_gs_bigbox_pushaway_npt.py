from os.path import isdir
import numpy as np
from cmm.md.cmmsystems import CMMSystem, angstroms
from cmm.tools.parsers import read_chargefile
from parmed.openmm.reporters import RestartReporter

''' BIG BOX. Long NPT, even longer NVT. 
    Also move counterions further away. 
    Use best vdw values from previous fit. 

    XXX WIP: PATHS NEED UPDATING TO NEW LOCATIONS.

    Do ES charges / structure based on this one.

    On HPC: 
       voltash
       source ~/.openmm
    '''


tag = 'agptpop_gs_bigbox_pushaway' 
chargefile = 'gs_constrained_chelpg_charges.dat' 

with open(chargefile, 'r') as f:
    lines = f.readlines()

#versions = [line for line in lines if ': ESP char' in line]
#for vers in versions:
#    print(vers)

# Gave best first round of fits in the small box, 
# according to Phillip, so let us do all EQ with that
ptval = '1.25' 
agval = '2.75'

print(f'{tag}_Pt{ptval}_Ag{agval}')

fpath = f'./data/AgPtPOP/{tag}_Pt{ptval}_Ag{agval}/'

if isdir(fpath):  # has already run
    print(f'{fpath} already exists, skipping...')
else:
    # New charges:
    charges, elmnts = read_chargefile(chargefile, f'Pt{ptval}_Ag{agval}')
    charges = charges[:, 1]
    print(f'Sum of charges: {sum(charges)}')
    idx = list(range(len(charges)))

    #### init
    sys = CMMSystem(f'{tag}_npt_Pt{ptval}_Ag{agval}',
                    fpath=fpath)

    # load prmtop (and in this case also inpcrd, which means positions will also be
    # loaded)
    sys.system_from_prmtop('./data/AgPtPOP/agptpop_solv_custom_big.prmtop',
                           inpcrd='./data/AgPtPOP/agptpop_solv_custom_big.inpcrd')
    # inpcrd has solute centered in cell. So we can hook that up

    # set new charges
    sys.adjust_solute_charges(idx, charges)

    # restrain solute and ct ions, move ct ions away
    ns_idx, syms = sys.get_nonsolvent_indices()
    ct_idx = [i for i, sym in enumerate(syms) if sym == 'Na+']
    solu_idx = [i for i, sym in enumerate(syms) if sym != 'Na+']

    solu_pos = sys.positions[solu_idx].value_in_unit(angstroms)
    ct_pos  = sys.positions[ct_idx].value_in_unit(angstroms)
    cen = np.mean(solu_pos, 0)
    vs = ct_pos - cen
    vs /= np.linalg.norm(vs, axis=1)
    for i, v in enumerate(vs):
        print(np.linalg.norm(ct_pos[i] - cen, axis=0)) 
        ct_pos[i] += 20 *v
        print(np.linalg.norm(ct_pos[i] - cen, axis=0)) 
        
    pos = sys.positions.value_in_unit(angstroms)
    pos[ct_idx] = ct_pos

    sys.positions = pos * angstroms

    sys.restrain_atoms(ns_idx, 500)

    # init NPT
    sys.add_barostat()

    # and integrator
    sys.set_integrator(timestep=1.0)  

    # init sim
    sys.init_simulation(platform='CUDA', double=False)

    # minimize
    sys.minimize()

    # MBD:
    sys.mbd(temperature=10)

    # reporters
    sys.standard_reporters()

    # restart reporter
    restrt = RestartReporter(fpath + f'{tag}_npt_Pt{ptval}_Ag{agval}.rst7', 1000) 
    sys.add_reporter(restrt)

    # run
    sys.run(time_in_ps=2000)


## and following NVT for longer
sys = CMMSystem(f'{tag}_nvt_Pt{ptval}_Ag{agval}',
                fpath=fpath)


sys.system_from_prmtop('./data/AgPtPOP/agptpop_solv_custom_big.prmtop')


sys.load_rst7(fpath + f'{tag}_npt_Pt{ptval}_Ag{agval}.rst7')

# set new charges
sys.adjust_solute_charges(idx, charges)

# restrain 
ns_idx, syms = sys.get_nonsolvent_indices()
sys.restrain_atoms(ns_idx, 500)

# and integrator
sys.set_integrator(timestep=2.0)  

# init sim
sys.init_simulation(platform='CUDA', double=False)

# reporters
sys.standard_reporters()

# restart reporter
restrt = RestartReporter(fpath + f'{tag}_nvt_Pt{ptval}_Ag{agval}.rst7', 1000) 
sys.add_reporter(restrt)

# run
sys.run(time_in_ps=50000)
