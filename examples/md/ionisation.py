from cmm.md.cmmsystems import CMMSystem
from parmed.openmm.reporters import RestartReporter
import simtk.unit as u
import numpy as np


''' Do 198 out-of-equilibrium runs from 198 "ground state"
    frames (rst7 files) containing positions and velocities,
    formatted as e.g. I-_v1x/vels.nvt.rst7.1 for the first one.
'''

tag = 'I-_v1x'  # Name of the "ground state" run
tag1 = 'ionisation'  # Name of the "excited state" runs
root_dir = './'

runs = np.arange(1, 200, 1)

for i in runs:  # you could also make a list of rst7 files instead...
	# init
	sys = CMMSystem(tag1 + f'_{i}_nve', fpath=root_dir + tag1)

	# load prmtop
	sys.system_from_prmtop('I-_solv.prmtop')

    # adjust charges
	sys.adjust_solute_charges([0,1], [0.,0.])
	sys.load_rst7(f'{tag}/vels_nvt.rst7.{i}')

	# and integrator
	sys.set_integrator(fc=0, timestep=1.0, ensemble='NVE')
	# init sim

	sys.init_simulation(double=True)

	# reporters
	sys.standard_reporters(step=1)  # save every frame. Will slow down simulation!

	# run
	sys.run(time_in_ps=3)
