package require pbctools 2.5;

# VMD and TCL SUCKS SO MUCH
dict set conv AG "Ag"
dict set conv PT "Pt"
dict set conv P1 "P"
dict set conv P2 "P"
dict set conv P3 "P"
dict set conv P4 "P"
dict set conv P5 "P"
dict set conv P6 "P"
dict set conv P7 "P"
dict set conv P8 "P"
dict set conv O "O"
dict set conv O1 "O"
dict set conv O2 "O"
dict set conv O3 "O"
dict set conv O4 "O"
dict set conv O5 "O"
dict set conv O6 "O"
dict set conv O7 "O"
dict set conv O8 "O"
dict set conv O9 "O"
dict set conv O10 "O"
dict set conv O11 "O"
dict set conv O12 "O"
dict set conv O13 "O"
dict set conv O14 "O"
dict set conv O15 "O"
dict set conv O16 "O"
dict set conv O17 "O"
dict set conv O18 "O"
dict set conv O19 "O"
dict set conv O20 "O"
dict set conv Na+ "Na"
dict set conv H1 "H"
dict set conv H2 "H"
dict set conv H3 "H"
dict set conv H4 "H"
dict set conv H5 "H"
dict set conv H6 "H"
dict set conv H7 "H"
dict set conv H8 "H"
dict set conv H9 "H"



proc dirlist { ext } {
    set contents [glob -type d $ext]
    foreach item $contents {
       append out $item
       append out "\n"
       }
    return $out
    }

proc flist { dir ext } {
    set contents [glob -directory $dir $ext]
    foreach item $contents {
       append out $item
       append out "\n"
       }
    return $out
    }


# not as crappy as VMDs
proc xyzwrite { sel outf conv } {

    # Shit crap BS VMD hast lost the elements on the cluster
    # Whereas it finds it fine locally.
    #set elements [$sel get element]
    set names [$sel get name]
    set xyz [$sel get {x y z}]
    set atms [llength $xyz]

    #puts $elements

    set out [open $outf w]
    puts $out "$atms"
    puts $out "Im so fancy!"

    set ct 0
    foreach f $xyz {
        set name [lindex $names $ct] 
        set el [dict get $conv $name]
        set x [lindex $f 0]
        set y [lindex $f 1]
        set z [lindex $f 2]

        puts $out [format "%3s%16.8f%16.8f%16.8f" $el $x $y $z]        

        incr ct
    }
    close $out

}

set dirs [dirlist *pushaway*]
#set dirs [dirlist *ptpop_pushaway_Pt2.00_Ag2.50];

foreach dir $dirs {
    puts $dir
    set dcds [flist $dir *nvt*.dcd]
    set dcd [lindex $dcds 0];  # there should only be one file per dir

    mol load parm7 agptpop_solv_custom.prmtop dcd $dcd
    pbc wrap -centersel "residue < 7" -center com -compound residue -all

    set trajlen [molinfo top get numframes]
    set sel [atomselect top "all and not name EPW"]	
    for {set i 0} {$i < $trajlen} {incr i 5} {
        $sel frame $i
        set onum [format "%04d" $i]
        puts $onum
        # writexyz that puts names instead of elements because its shit:
        #$sel writexyz $dir/$onum.xyz 
        xyzwrite $sel $dir/$onum.xyz $conv

    }
}

