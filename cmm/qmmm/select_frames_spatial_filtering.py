''' This script selects frames of MD trajectories based on
    a spatial filtering approximation of excitation by a
    Gaussian ultrashort pulse (see eqs. 16 and 17 from
    Levi JPCC 122, 7100 (2018)).

    Frames are selected according to an initial excited-state
    distribution given by:
    Pes = F^2 * Pgs
    where F is an excitation window depending on the
    difference between ground- and excited-state potentials
    and the pulse parameters:
    F = alpha * exp(-(tau^2 * (dV - hbar * omega1)^2) / (2 * hbar^2))

    If an excitation fraction is provided, Pes is scaled by
    the excitation fraction, otherwise the Pes is scaled such
    that the ration between Pes and Pgs is always smaller than
    1/3 (a lineraly polarized pulse cannot excite more than 1/3
    of the population).
'''

import os
import glob
import random

import numpy as np

import matplotlib.pyplot as plt
import matplotlib

import ase.units as units
from ase.db import connect
from ase.io import Trajectory


use_pulse = True  # True if you want to use the excitation pulse
                  # to select frames, otherwise select frames randomly
                  # from the ground-state distribution
T = 300  # Temperature in K
n_es_trajs = 75  # Number of frames to select
read_trajs = False  # True if you want to create a dictionary
                    # of distances between atom 0 and atom 1
                    # by reading .traj files
                    # If False, the distances will be read from
                    # a specified npy file
i0 = 0  # Index of atom 0 in the .traj files
i1 = 1  # Index of atom 1 in the .traj files
path_to_trajs = './'  # Path to directory with .traj files
path_to_dict = '../../examples/qmmm/'   # Path to directory with npy file
                                        # containing dictionary of atomic
                                        # distances from trajectories
flname_dict = 'ptpop-tip4p_dptpt_oldgsqmmm.npy'  # Name of file with
                                                 # dictionary of atomic distances

'''Define here the parameters of the Morse potentials: 
   V = de*(1-exp(-a(r-re)))^2'''
# Parameters to define an array of distances
# for x-axis of potentials and distributions
r_step = 0.001  # For analytical potentials and distributions
r_step_samp = 0.005  # For the distribution of sampled points
rlow = 2.70
rhigh = 3.30
# Parameters of Morse potential fits to thermal distributions
# of Pt-Pt distances from simulations of the PtPOP complex in
# water (Levi JPCC 122, 7100 (2018))
# Ground state
degs = 2.63  # Depth of the potential in eV
ags = 1.02   # Parameter a in Ang-1
regs = 2.98  # Equilibrium distance in Ang
# Excited state
dees = 2.60
aes = 1.15
rees = 2.78
max_exp_abs = 3.35  # Position of the maximum of the experimental
                    # absorption spectrum in eV

'''Define here the parameters of the excitation window:
   F = alpha * exp(-(tau^2 * (dV - hbar * omega1)^2) / (2 * hbar^2))
   '''
hbar = 6.582119569e-16  # hbar constant in eVs
tau = 60e-15            # Duration in s
e1 = 3.35               # Energy (hbar*omega1) in eV
alpha = None            # Excitation fraction, use None if not known


def create_morse_potential(r, de, a, re):
    morse_potential = de * (1 - np.exp(- a * (r - re)))**2
    
    return morse_potential

def create_dict_from_trajs(path_to_trajs, i1=0, i2=1):
    d = {}
    # Load trajectories and save distances
    for fl in os.listdir(path_to_trajs):
        print(fl)
        if fl.endswith('.traj'):
           d[fl] = []
           traj = Trajectory(path_to_trajs + fl)
           for atoms in traj:
               atoms.constraint = []
               d[fl].append(atoms.get_distance(i1, i2))

    return d

def select_frames_from_dict(d, d_samp, reject=250):
    d_rej = {}
    for key in d.keys():
        d_rej[key] = np.asarray([True for i in d[key]])

    selected_frames = {}
    selected_frames['traj_names'] = []
    selected_frames['steps'] = []
    selected_frames['d'] = []

    for i in range(len(d_samp)):
        picked = False
        while not picked:
            # Pick a random frame of a random trajectory
            trajname, trajds = random.choice(list(d.items()))
            step = np.random.randint(len(trajds))

            # Select frame if the distance is consistent
            # with the value we are looking for
            if (np.around(trajds[step], 3) == d_samp[i]) and d_rej[trajname][step]:
                selected_frames['traj_names'].append(trajname)
                selected_frames['steps'].append(step)
                selected_frames['d'].append(trajds[step])

                # We don't want to select frames that are close
                # to each other. Reject frames around the
                # selected point, so they are not picked later
                if step - reject < 0:
                    lowlim = None
                else:
                    lowlim = step - reject
                if step + reject > len(trajds):
                    highlim = None
                else:
                    highlim = step + reject
                d_rej[trajname][lowlim:highlim] = False

                picked = True

    # Check that we didn't select frames close to each other
    for i, traj1 in enumerate(selected_frames['traj_names']):
        for j, traj2 in enumerate(selected_frames['traj_names'][i + 1:]):
            if traj1 == traj2:
                assert abs(selected_frames['steps'][i] - selected_frames['steps'][i + 1:][j]) > reject - 1

    return selected_frames

def plot_lines(ax, x, y, labels, colors=None, 
               xlim=None, ylim=None):
    if colors is None:
        colors = ['k', 'r', 'b', 'b']
    
    for i in range(len(y)):
        ax.plot(x, y[i], '-', color=colors[i],
                linewidth=2, label=labels[i])
        
    if xlim is not None:
        ax.set_xlim(xlim[0], xlim[1])
    if ylim is not None:
        ax.set_ylim(ylim[0], ylim[1])


'''Define Morse potentials.'''
r = np.arange(rlow, rhigh, r_step)  # Array of distances for x-axis

pot_gs = create_morse_potential(r, degs, ags, regs)
pot_es = create_morse_potential(r, dees, aes, rees)

# We shift the es potential such that the difference 
# between the two potentials is equal to the position
# of the maximum of the experimental abs. spectrum
de_gsreq = dees * (1 - np.exp(- aes * (regs - rees)))**2
pot_es += max_exp_abs - de_gsreq

'''Create dictionary of distances from .traj files or read 
   it from file.'''
if read_trajs:
    d = create_dict_from_trajs(path_to_trajs, i0, i1)
else:
    d = np.load(path_to_dict+flname_dict,
                allow_pickle='TRUE').item()

'''Make excited-state distribution using the Boltzmann 
   distribution of the ground-state potential and a pulse 
   excitation window.'''
# Make normalized Boltzmann distribution
p_gs_boltz = np.exp(- pot_gs / (units.kB * T))
p_gs_boltz /= np.sum(p_gs_boltz * r_step)

# Make window function
if use_pulse:
    F = np.exp(- (tau**2 * (pot_es - pot_gs - e1)**2) / (2 * hbar**2))
else:
    F = 1

# Make excited-state distribution
p_es = F**2 * p_gs_boltz

if alpha is not None:
    # Scale excited-state distribution to achieve
    # excitation fraction alpha
    a = alpha / np.sum(p_es * r_step)
    p_es *= a

    # The distribution should integrate to the excitation fraction
    assert np.sum(p_es * r_step) - alpha < 1e-6
else:
    # Find excitation fraction based on selection rules
    # for linearly polarized pulse
    pratiomax = np.argmax(p_es / p_gs_boltz)
    p_es = p_es / p_es[pratiomax] * 1 / 3. * p_gs_boltz[pratiomax]

    print('Excitation fraction: %.3f' % (np.sum(p_es * r_step)))

'''Select frames of ground-state trajectories.
   Random sampling according to excited-state distribution.'''
# Normalize the distribution to sum to 1
p_es /= np.sum(p_es)

# Sample the distribution and round up values
d_samp = [np.around(np.random.choice(r, p=p_es), 3) for i in range(n_es_trajs)]

# Select frames from ground-state trajectories
selected_frames = select_frames_from_dict(d, d_samp, reject=250)

'''Plot distributions.'''
# Make distribution of selected frames
r_samp = np.arange(rlow, rhigh, r_step_samp)
r_bins_samp = np.arange(r_samp[0] - r_step_samp / 2,
                        r_samp[-1] + r_step_samp, r_step_samp)
p_selected, bin_edges_selected = np.histogram(selected_frames['d'],
                                              bins=r_bins_samp,
                                              density=True)
# Normalize distributions for plotting
p_es /= np.sum(p_es * r_step)

# Plot distributions
f, ax = plt.subplots(figsize=(7,6))
plot_lines(ax, r_samp, [p_selected], ['es distribution sampling'], colors='r')
plot_lines(ax, r, [p_es], ['es distribution theoretical ', 'sampling'])
ax.set_xlabel('d (Å)', fontsize=18, fontweight='normal')
ax.set_ylabel('probability distribution', fontsize=18, fontweight='normal')
lgd=plt.legend(loc='upper left', fontsize=14)
lgd.draw_frame(False)
f.tight_layout()
plt.show()

'''Write selected frames to file.'''
with open('selected_frames.txt', 'w') as flout:
    for i in range(len(selected_frames['traj_names'])):
        flout.write('%s %6d %10.6f\n' % (selected_frames['traj_names'][i],
                                         selected_frames['steps'][i],
                                         selected_frames['d'][i]))
