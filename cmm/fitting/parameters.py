import os
import numpy as np
from ase import units
from ase.io import read
from ase.calculators.qmmm import LJInteractionsGeneral
from ase.calculators.tip4p import sigma0, epsilon0
from ase.calculators.acn import (sigma_c, sigma_me, sigma_n,
                                 epsilon_c, epsilon_me, epsilon_n)

# -------------------------------------Fe(bpy)3, OPLS:
C_eps = 0.00329567
C_sig = 3.55000 
N_eps = 0.00737190
N_sig = 3.25000 
Fe_eps = 0.00056373
Fe_sig = 2.59400 
H_eps = 0.00130092
H_sig = 2.42000 

lj_fe_gs_opls = {'C':  (C_eps, C_sig),
                'N':  (N_eps, N_sig),
                'Fe': (Fe_eps, Fe_sig),
                'H': (H_eps, H_sig)}


################################## TIP4P and Guardia
sig_h2o = np.array([sigma0, 0, 0])
eps_h2o = np.array([epsilon0, 0, 0])

sig_acn = np.array([sigma_me, sigma_c, sigma_n])
eps_acn = np.array([epsilon_me, epsilon_c, epsilon_n])


# UFF, Rappe et al. JACS 1992
epsilonP_qm = 0.305 * units.kcal / units.mol
sigmaP_qm = 3.694
epsilonO_qm = 0.060 * units.kcal / units.mol
sigmaO_qm = 3.118
epsilonH = 0.044 * units.kcal / units.mol
sigmaH = 2.571
epsilonN_qm = 0.069 * units.kcal / units.mol
sigmaN_qm = 3.260
epsilonC_qm = 0.105 * units.kcal / units.mol
sigmaC_qm = 3.431

here = os.path.dirname(__file__)  
################################## lj object for PtPOP-H2O:
atoms_ptp = read(here + '/../../data/fitting/ptpop.xyz')
qmidx_ptp = list(range(len(atoms_ptp)))
lj_parms_ptpop = {('Pt'): (0.080 * units.kcal / units.mol, 2.454),
                  ('P'): (epsilonP_qm, sigmaP_qm),
                  ('O'): (epsilonO_qm, sigmaO_qm),
                  ('H'): (epsilonH, sigmaH)}
epsilon_qm = np.array([lj_parms_ptpop[a.symbol][0] for a in atoms_ptp[qmidx_ptp]])
sigma_qm = np.array([lj_parms_ptpop[a.symbol][1] for a in atoms_ptp[qmidx_ptp]])
lj_PtPH2O = LJInteractionsGeneral(sigma_qm, epsilon_qm, 
                                  sig_h2o, eps_h2o, 
                                  len(qmidx_ptp))

################################## lj object for PtPOP-ACN:
lj_PtPACN = LJInteractionsGeneral(sigma_qm, epsilon_qm, 
                                  sig_acn, eps_acn, 
                                  len(qmidx_ptp))


################################## lj object for IrDimen-H2O:
atoms_ird = read(here + '/../../data/fitting/ird.xyz')
qmidx_ird = list(range(len(atoms_ird)))
lj_parms_ird = {('Ir'): (0.0190, 1.487),
               ('N'): (epsilonN_qm, sigmaN_qm),
               ('C'): (epsilonC_qm, sigmaC_qm),
               ('H'): (epsilonH, sigmaH)}

epsilon_qm = np.array([lj_parms_ird[a.symbol][0] for a in atoms_ird[qmidx_ird]])
sigma_qm = np.array([lj_parms_ird[a.symbol][1] for a in atoms_ird[qmidx_ird]])
lj_IrDH2O = LJInteractionsGeneral(sigma_qm, epsilon_qm, 
                                  sig_h2o, eps_h2o, 
                                  len(qmidx_ird))

################################## lj object for IrDimen-ACN:
lj_IrDACN = LJInteractionsGeneral(sigma_qm, epsilon_qm, 
                                  sig_acn, eps_acn, 
                                  len(qmidx_ird))



################################## lj object for FeTPY-H2O:
# Fe: https://pubs.acs.org/doi/pdf/10.1021/jp407739h
epsilonFe = 1.200 * units.kJ / units.mol
sigmaFe = 2.11
atoms_fet = read(here + '/../../data/fitting/fetpy.xyz')
qmidx_fet = list(range(len(atoms_fet)))
lj_parms_fet = {('Fe'): (epsilonFe, sigmaFe),
                ('N'): (epsilonN_qm, sigmaN_qm),
                ('C'): (epsilonC_qm, sigmaC_qm),
                ('H'): (epsilonH, sigmaH)}
epsilon_qm = np.array([lj_parms_fet[a.symbol][0] for a in atoms_fet[qmidx_fet]])
sigma_qm = np.array([lj_parms_fet[a.symbol][1] for a in atoms_fet[qmidx_fet]])
lj_FeTH2O = LJInteractionsGeneral(sigma_qm, epsilon_qm, 
                                  sig_h2o, eps_h2o, 
                                  len(qmidx_fet))
