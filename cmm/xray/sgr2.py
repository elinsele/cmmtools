import os, re, copy
import numpy as np
from .debye import Debye
from .sgr import Damping
from scipy.optimize import least_squares
from scipy.stats import linregress
import matplotlib.pyplot as plt

class RDF:
    ''' RDF Object.

        r: np.array
            r vector
        g: np.array, same length as r
            g(r) values
        name1: str
            Name of atomtype/element of 'left' atoms
        name2: str
            Name of atomtype/element of 'right' atoms
        region1: str, 'solute' or 'solvent'
            Which region does the 'left' atom belong to
        region2: str, 'solute', or 'solvent'
            Which region does the 'right' atom belong to.

    '''

    def __init__(self, r, g, name1, name2,
                             region1, region2,
                             n1=None, n2=None,
                             damp=None, volume=None,
                             r_max=None, r_avg=None):
        self.r = r
        self.g = g
        self.name1 = name1
        self.name2 = name2
        self.region1 = region1
        self.region2 = region2

        self.n1 = n1  # number of 'left' atoms
        self.n2 = n2  # number of 'right' atoms

        self.diagonal = (name1 == name2) & (region1 == region2)
        self.cross = (region1 != region2)

        self.damp = damp  # Damping object

        self.volume = volume  # Volume used for the N/V RDF normalization
                              # (Volume of your sim. box)

        self.r_max = r_max  # Stop integrating here
        self.r_avg = r_avg  # Use avg. g(r > r_avg) for g0

        self.s = None  # Structure factor. Calculated with SGr. WIP: Move to RDF simply?


class RDFSet(dict):
    ''' Container object for a set of RDFs.
        Basically a Dict with a few more options '''

    def __init__(self, *arg, **kwargs):
        self.rdfs = {}
        super(RDFSet, self).__init__(*arg, **kwargs)

    def add_flipped(self, rdf):
        r_rdf = copy.copy(rdf)
        r_rdf.name2 = rdf.name1
        r_rdf.name1 = rdf.name2
        r_rdf.region2 = rdf.region1
        r_rdf.region1 = rdf.region2
        self.rdfs[(r_rdf.name1, r_rdf.region1,
                   r_rdf.name2, r_rdf.region2)] = r_rdf

    def __len__(self):
        return len(self.rdfs)


class SGr:
    ''' X-Ray Scattering from pairwise radial distribution functions.

        Can divide signal up in Solute-Solute (u-u), Solute-Solvent (u-v)
        and Solvent-Solvent (v-v) contributions.

        See e.g.: DOI: 10.1088/0953-4075/48/24/244010

    '''

    def __init__(self, volume, qvec=np.arange(0, 10, 0.01)):
        self.qvec = qvec
        self.v = volume


    def structure_factor(self, rdf):
        ''' Calculate the structure factor for a given RDF '''
        r = rdf.r
        g = rdf.g
        dr = r[1] - r[0]

        avg = 1
        if rdf.r_avg is not None:
            avg = np.mean(g[r > rdf.r_avg])

        if rdf.r_max is not None:
            mask = np.zeros(len(r), bool)
            mask[r < rdf.r_max] = True
            r = r[mask]
            g = g[mask]

        qr = self.qvec[:, None] * r[None, :]
        r2sin = 4 * np.pi * dr * (np.sinc(qr / np.pi)) * r[None, :]**2

        if (rdf.region1.lower() == 'solute') and (rdf.region2.lower() == 'solute'):
            g0 = 0
        else:
            g0 = 1  

        h = g - g0 * avg
        if rdf.damp is not None:
            h *= rdf.damp.damp(r)

        kd = int((rdf.region1 == rdf.region2) and (rdf.name1 == rdf.name2))

        ### XXX WIP: Change to partial S(Q)
        s = 1 + ((rdf.n2 - kd) / self.v) * (r2sin @ h)

        # update the rdf inplace with structure factor
        rdf.s = s

        return s

    def scoh(self, rdf):
        ''' Coherent X-Ray Scattering'''
        kdr = int((rdf.region1 == rdf.region2))
        kd = int((rdf.region1 == rdf.region2) and (rdf.name1 == rdf.name2))
        s = self.structure_factor(rdf)

        # Get form factors
        self.atomic_f0(rdf)

        term_1 = (rdf.n1 * rdf.f0_1 * rdf.f0_2 * kd) * kdr  # no first term for cross terms
        term_2 = (s - 1) * rdf.n1 * rdf.f0_1 * rdf.f0_2

        self.term_1 = term_1
        self.term_2 = term_2

        s_coh = term_1 + term_2

        return s_coh

    def sdv(self, rdfs_l: list) -> np.array:
        ''' Displaced Volume (DV) Scattering.
            Needs all cross RDFs from solute l to solvent atom types v
        '''

        r = rdfs_l[0].r
        g = rdfs_l[0].g
        dr = r[1] - r[0]

        fdv = np.zeros(len(self.qvec))

        qr = self.qvec[:, None] * r[None, :]
        r2sin = 4 * np.pi * dr * (np.sinc(qr / np.pi)) * r[None, :]**2
        g0 = 1  # XXX WIP delta RDFs, long-r average

        for rdf in rdfs_l:
            avg = 1
            if rdf.r_avg is not None:
                avg = np.mean(g[r > rdf.r_avg])

            h = g - g0 * avg
            if rdf.damp is not None:
                h *= rdf.damp.damp(r)

            fdv += (rdf.n2 / self.v) * (r2sin @ h)


    def atomic_f0(self, rdf):
        ''' Calculate atomic form factors and
            update RDF object in place '''
        db = Debye(self.qvec)
        rdf.f0_1 = db.atomic_f0(rdf.name1)
        rdf.f0_2 = db.atomic_f0(rdf.name2)


class DVOpt:
    '''
        Class for fitting displaced volumes to obtain converged KB Integrals.
        This will make the low-q calculation more robust.

        It finds the radius Ri which gives the spherical volume where j particles
        cannot find themselves due to particle i, from which the RDF sampling starts,
        which is missing for correctly normalizing the RDF to go to 1 in the asymptotic
        limit.
        It is a good idea to check the found volume makes sense wrt the distance before
        the RDF gets any amplitude.


        rdf: cmm.xray.sgr2.RDF object
             The object needs a ".volume"-attribute

        fit_strt: float
            At which R the fitting should start: Preferably as soon as the local structure
            has died out.

        fit_stop: float
            Where to stop the fitting region
    '''
    def __init__(self, rdf, fit_strt=25, fit_stop=40, rmaxes=np.arange(5, 40), ctype: str='VC') -> None:
        self.rdf = rdf
        self.fit_strt = fit_strt
        self.fit_stop = fit_stop
        self.rmaxes = rmaxes
        self.sgr = SGr(rdf.volume, qvec=np.arange(0, 0.1, 0.1))

        self.x = None
        self.total_opt = None

        mask = np.zeros(len(rmaxes), bool)
        mask[(rmaxes > fit_strt) & (rmaxes < fit_stop)] = True
        self.mask = mask

        if ctype == 'VC':
            self.correct = self.volume_correct
        elif ctype == 'PE':
            self.correct = self.perera_correct
        else:
            raise ValueError('Did not understand RDF correction model')

    def get_s0(self):
        s0 = np.zeros((len(self.sgr.qvec), len(self.rmaxes)))
        for j, r_max in enumerate(self.rmaxes):
            rdf = copy.deepcopy(self.rdf)

            damp = Damping('simple', L=r_max)

            rdf.damp = damp
            rdf.r_max = r_max

            rdf.g = self.correct(self.x)

            s0[:, j] = self.sgr.structure_factor(rdf)

        self.s0 = s0
        return s0

    def volume_correct(self, Ri, rdf=None):
        if rdf is None:
            rdf = copy.deepcopy(self.rdf)
        kd = int((rdf.region1 == rdf.region2) and (rdf.name1 == rdf.name2))
        V = rdf.volume
        N = rdf.n2

        Vnew = V - (4/3) * Ri**3 *np.pi
        fac = (Vnew/V) * (N / (N - kd))
        return rdf.g * fac

    def perera_correct(self, Ri):
        rdf = copy.deepcopy(self.rdf)
        kappa = 2 * Ri
        alpha = 3 * Ri
        g0_avg = np.mean(rdf.g[(rdf.r > self.fit_strt) & (rdf.r < self.fit_stop)])
        #g0_avg = np.mean(rdf.g[rdf.r > 35])
        fac = 1 + ((1 - g0_avg) / 2) * (1 + np.tanh((rdf.r - kappa) / alpha))
        return rdf.g * fac


    def residual(self, x):
        self.x = x  # set new Ri

        mask = self.mask

        s0 = self.get_s0()
        x0 = np.mean(s0[:, mask])

        opt = least_squares(self.flat_obj, x0)
        self.flat_opt = opt

        y = np.zeros(mask.sum()) + self.flat_opt.x

        return 1e5 * ((s0[:, mask] - y) / mask.sum()).flatten()

    def flat_obj(self, x):
        nmask = self.mask.sum()
        y = np.zeros(nmask) + x

        res = (self.s0[:, self.mask] - y)**2 / nmask

        return res.flatten()

    def fit(self, x0):
        opt = least_squares(self.residual, x0)
        self.total_opt = opt
        return opt

    def fit2(self, x0, residual=None):
        if not residual:
            opt = least_squares(self.residual2, x0)
        else:
            opt = least_squares(residual, x0)
        self.total_opt = opt
        return opt

    def fit_rdf(self, x0):
        ''' Simply find an Ri that forces the avg g(r) be 1.
            Beware that you will get STRANGE results if the RDF is NOT actually
            converged to its (1 + eps) asymptotic value '''

        def rdfres(x0):
            rdf = copy.deepcopy(self.rdf)
            rdf.g = self.correct(x0[0])
            mask = np.zeros(len(rdf.g), bool)
            mask[(rdf.r > self.fit_strt) & (rdf.r < self.fit_stop)] = True
            res = (rdf.g[mask]  - 1) * 1e8
            return res

        opt = least_squares(rdfres, x0)
        self.total_opt = opt
        return opt

    def residual2(self, x0: np.array):
        ''' Fit both at the same time '''
        new_ri = x0[0]
        b = x0[1]
        mask = self.mask

        self.x = new_ri
        self.get_s0()

        y = np.zeros(mask.sum()) + b

        res =  1e5 * (self.s0[:, self.mask] - y)**2 / mask.sum()

        # also fit a straight line and penalize the fit on the slope
        lr = linregress(self.rmaxes[mask], self.s0[:, mask])
        self.lr = lr
        punish_slope = 1e5 * np.ones(mask.sum()) * lr.slope**2
        # WIP: The other reguralizer could be distance away from first RDF amplitude.

        self.resvals = res.flatten()
        self.punish_slope = punish_slope
        return res.flatten() + punish_slope


    def residual3(self, x0):
        new_ri = x0[0]
        self.x = new_ri

        b = x0[1]
        r_max = x0[2]
        r_cut = r_max - 2

        rdf = copy.deepcopy(self.rdf)

        mask = np.zeros(len(rdf.g), bool)
        mask[(rdf.r > self.fit_strt) & (rdf.r < self.fit_stop)] = True

        avg = np.mean(rdf.g[mask])

        smooth = Damping('zederkof', r_max=r_max, r_cut=r_cut)
        self.rdf.g = (rdf.g - avg) * smooth.damp(self.rdf.r) + avg
        self.get_s0()  # this makes the correction

        mask = self.mask
        y = np.zeros(mask.sum()) + b

        res =  1e8 * (self.s0[:, self.mask] - y)**2 / mask.sum()
        return res.flatten()




    def plot(self, fig=None):
        if not fig:
            fig, ax = plt.subplots(1,1, figsize=(9, 4))
        else:
            (fig, ax) = fig

        name = self.rdf.name1 + '-' + self.rdf.region1 + '-' + \
               self.rdf.name2 + '-' + self.rdf.region2

        ax.plot(self.rmaxes, self.s0.flatten(), label=f'{name}: {self.total_opt.x[0]:8.6f}')
        ax.plot(self.rmaxes[self.mask],
                np.zeros_like(self.rmaxes[self.mask]) + self.flat_opt.x, 'k--')
        ax.legend(loc='best')
        return ax


## Helper functions
def load_rdfs(rdflist, stoich=None, damp=None):
    ''' Make a list of RDF objects from a list of dat files from e.g. VMD

        The list should contain paths to files in the following format:

            gX_a-Y_b.dat

        where X,Y is the atom type, and a,b is either u or v, for solUte
        and solVent, respectively.

        and the .dat files should contain (r, gr) columns.

        If a stoichometry dict is provided, the n1 and n2
        of the rdf objects will also be filled.

    '''

    rdfs = []
    trslt = {'c': 'cross', 'u':'solute', 'v':'solvent'}

    for rdffile in rdflist:
        name = rdffile.split(os.sep)[-1]
        # use reg exp since atom types can be 1 or 2 chars long:
        atyp1 = re.search('g(.*?)_', name).group(1)
        part1 = re.search(atyp1 + '_(.*?)-', name).group(1)

        atyp2 = re.search('-(.*?)_', name).group(1)
        part2 = re.search('-' +  atyp2 + '_(.*).dat', name).group(1)

        dat = np.genfromtxt(rdffile)
        rdf = RDF(dat[:, 0], dat[:, 1],
                  atyp1, atyp2,
                  trslt[part1], trslt[part2], damp=damp)

        if stoich:
            rdf.n1 = stoich[(atyp1, trslt[part1])]
            rdf.n2 = stoich[(atyp2, trslt[part2])]

        rdfs.append(rdf)

    return rdfs

def rdfset_from_dir(dirname, prmtop):
    import MDAnalysis as mda
    import glob

    rdf_files = sorted(glob.glob(dirname + os.sep + '*dat'))

    whered = {'u':'solute', 'v':'solvent', 'c':'cross'}
    # Load universe to get stoichometry.
    u = mda.Universe(prmtop)

    # Unfortunately mda cannot get the box size from the prmtop, even though it is there
    with open(prmtop, 'r') as f:
        lines = f.readlines()
    for l, line in enumerate(lines):
        if 'FLAG BOX_DIMENSIONS' in line:
            box = lines[l + 2]
    box = [float(x) for x in box.split()[1:] ]
    V = np.prod(box)

    set_uc = RDFSet()  # Uncorrected

    for f, file in enumerate(rdf_files):
        data = np.genfromtxt(file)
        r = data[:, 0]
        g = data[:, 1]

        # find elements from file name. This is very specific to this exact naming scheme...
        el1 = re.search('g(.*?)_', file).group(1)
        part1 = re.search(el1 + '_(.*?)-', file).group(1)
        where1 = whered[part1]

        el2 = re.search('-(.*?)_', file).group(1)
        part2 = re.search('-' +  el2 + '_(.*).dat', file).group(1)
        where2 = whered[part2]

        Ni = len([atom for atom in u.atoms if el1 in atom.name])
        Nj = len([atom for atom in u.atoms if el2 in atom.name])

        rdf = RDF(r, g, el1, el2, where1, where2, n1=Ni, n2=Nj)
        rdf.volume = V
        set_uc[(rdf.name1, rdf.region1, rdf.name2, rdf.region2)] = rdf

    return set_uc

