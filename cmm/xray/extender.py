import numpy as np
from scipy.optimize import minimize 
from scipy.integrate import simps, trapz
from .sgr import Damping

class Extender:
    def __init__(self, new_start=15, step_len=2, avg_end=25, new_end=100, guess=15):
        self.new_start = new_start  # From which r value to start extension, and averaging 
        self.step_len = step_len    # Length of smooth step to the new average
        self.avg_end = avg_end      # Where to stop the averaging (e.g. end of sampled RDF) 
        self.new_end = new_end      # Extended RDF length. 
        self.avg = None
        self.rdf = None
        self.guess = guess
        
        self.h = None  # (g(r) - 1) * w(r)

    def __repr__(self):
        return 'Extender'

    @property
    def rdf(self):
        return self._rdf
    
    @rdf.setter
    def rdf(self, arr):
        self._rdf = arr
        
    def get_avg(self, r_min, r_max):
        r = self._rdf[:, 0]
        g = self._rdf[:, 1]
        
        self.avg = np.mean(g[(r > r_min) & (r < r_max)])
        
    def extend(self, new_end=100):
        rdf = self.rdf.copy()
        r = rdf[:, 0]
        r_end = r[-1]
        dr = r[1] - r[0]
        r_long = np.arange(r_end + dr, new_end + dr, dr)
        g_long = np.zeros(len(r_long)) + self.avg
        
        rdf_long = np.column_stack((r_long, g_long))
        rdf_long = np.vstack((rdf, rdf_long))
        
        self.rdf_long = rdf_long
        
    def replace(self):
        ''' Smoothly steps to average density from the end of the step
            to avg_end, and extends this density to new_end. '''

        new_start = self.new_start
        step_len = self.step_len
        avg_end = self.avg_end
        new_end = self.new_end
        
        rdf = self.rdf.copy()
        r = rdf[:, 0]
        g = rdf[:, 1]
        dr = r[1] - r[0]
        self.get_avg(new_start + step_len, avg_end)

        r_long = np.arange(r[0], new_end + dr, dr)
        org_mask = np.zeros(len(r_long), bool)
        org_mask[r_long <= new_start + step_len] = True

        g_long = np.zeros(len(r_long)) + self.avg
        g_long[org_mask] = g[r < new_start + step_len]

        damp = Damping('zederkof', r_cut=new_start, r_max=new_start + step_len)
        weight = damp.damp(r_long)
        g_long_damp = (g_long - self.avg) * weight + self.avg

        self.weight = weight
        self.rdf_long = np.column_stack((r_long, g_long_damp))
        
    def int_cn(self, h=None):
        if h is None:
            h = self.h
        r = self.rdf_long[:, 0]
        cn = trapz(4 * np.pi * r**2 * h, dx=r[1] - r[0])
        
        return cn
    
    def fit(self):
        #opt = minimize(self.cost, (27, 2), bounds=[(25, 100), (1, 75)])
        opt = minimize(self.cost, (self.guess, 2), bounds=[(self.avg_end, self.new_end), (1, self.avg_end)])
        self.opt = opt
        print(f'Cutoff-fitted Integral: {opt.fun:g}, r_cut: {opt.x[0] - opt.x[1]:6.4f}, r_max: {opt.x[0]:6.4f}')

        self.fit_damp = Damping('zederkof', r_cut=opt.x[0] - opt.x[1], 
                                r_max=opt.x[0])
        
    def cost(self, x, *args):
        r_max = x[0]
        r_cut = r_max - x[1]
        r = self.rdf_long[:, 0]
        g = self.rdf_long[:, 1]
        damp = Damping('zederkof', r_cut=r_cut, r_max=r_max)
            
        self.h = (g - 1) * damp.damp(r)
        cn = self.int_cn() 
        return cn**2