import numpy as np
from cmm.xray.debye import Debye
from ase.io import read
from ase import Atoms

''' Test that the Debye and numba implementations give the same
    and that the f0 list is updated if the atoms change.

    Using one of the coordinate files in the data dir. '''


fw_atoms = read('../data/md/agptpop/agptpop_gs_constrained_almost_opt_resequenced.xyz')
syms = fw_atoms.get_chemical_symbols()
syms.reverse()
rw_atoms = Atoms(''.join(syms), positions=np.flip(fw_atoms.positions))
deb = Debye()
s_fw = deb.debye(fw_atoms)
s_rw = deb.debye(rw_atoms)
assert max(s_fw - s_rw)  < 1e-9

s_nb = deb.debye_numba(rw_atoms)
assert max(s_fw - s_nb)  < 1e-9